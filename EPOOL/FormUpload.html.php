<html>
<link rel="stylesheet" media="(min-width: 640px)" href="min-640px.css">

  <head>
    <title>Epool homepage</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
      maximum-scale=1.0, minimum-scale=1.0">
      <!-- fogli di stile -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
      <!-- Inserimento header -->
      <?php include 'header.php'; ?>

      <div class="container" id="divContainer">
          <table class="table table-dark" style="background: rgba(0,0,0,0.5); ">
            <thead class="thead-dark">
              <tr align="center">
                <!--header della tabella-->
                <th colspan="5" scope="col">AGGIUNGI FOTO AL TUO PROFILO</th>
              </tr>
              <tr>
        <th><form action="upload.php" method="post" enctype="multipart/form-data">
          Seleziona l'immagine da caricare:
          <input type="file" name="fileToUpload" id="fileToUpload">
          <input type="submit" value="Carica" name="submit"></th>
        </tr>
        </form>
      </thead>
    </table>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <?php include "footer.php"; ?>
  </body>
</html>
