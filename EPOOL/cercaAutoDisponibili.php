<?php
//connessione al database mongodb
require 'mongodb.inc.php';
//connessione al db
require 'db.inc.php';
//funzione per creare finestra di alert javascript
function alert($msg) {
   echo "<script type='text/javascript'>alert('$msg');</script>";
}
try {
  $query = 'CALL listaAutoDisponibili()';
  $stmt = $pdo -> prepare($query);
  if (!$stmt -> execute()) {
    echo "Errore nella query". $dbc -> error.".";
  } else {
    //lista auto disponibili
    $resp = $stmt -> fetchAll(PDO::FETCH_ASSOC);
  }
} catch (Exception $e) {
  echo "Errore: ".$e.".";
  exit();
}

include 'formCercaAuto.html.php';
