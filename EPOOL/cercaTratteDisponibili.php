<?php
//connessione al database mongodb
require 'mongodb.inc.php';
//connessione al db
require 'db.inc.php';
//funzione per creare finestra di alert javascript
function alert($msg) {
   echo "<script type='text/javascript'>alert('$msg');</script>";
}

if (isset($_POST['action']) and $_POST['action'] == 'CERCA TRAGITTO') {

  //indirizzo di partenza scelto dall'utente
  $IndirizzoPartenza = $_POST['IndirizzoPartenza'];

  try {
        $query = 'CALL cercaTratteDisponibili(?)';
        $stmt = $pdo -> prepare($query);
        $stmt -> bindParam(1, $IndirizzoPartenza);
        if (!$stmt -> execute()) {
          echo "Errore nella query". $dbc -> error.".";
        } else {
          //lista tratte disponibili
          $resp = $stmt -> fetchAll(PDO::FETCH_ASSOC);
        }

  } catch (Exception $e) {
    echo "Errore nella ricerca tragitto: ".$e -> getMessage();
    exit();
  }

  include 'formTratteDisponibili.html.php';

} else {
  header("Location: index.php");
}
