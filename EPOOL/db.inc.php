<?php
//connessione al db
try {
   $pdo = new PDO('mysql:host=localhost;dbname=EPOOL','epooluser', 'MyP4s5w0rd!');
   $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   $pdo -> exec('SET NAMES "utf8"');
}
catch(PDOException $e) {
   echo("[ERRORE] Connessione al DB non riuscita. Errore: ".$e->getMessage());
   exit();
}
?>
