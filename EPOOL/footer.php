<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-4 text-center">
        <hr class="light">
        <h5>About</h5>
        <hr class="light">
        <p>Seguici sui nostri social</p>
        <ul class="footer-social list-inline">
          <li><a href="https://www.facebook.com/"><i class="fab fa-facebook"></i></a>
          <a href="https://www.instagram.com/?hl=it"><i class="fab fa-instagram"></i></a>
          <a href="https://twitter.com/"><i class="fab fa-twitter"></i></a>
          <a href="https://www.youtube.com/?gl=IT&hl=it"><i class="fab fa-youtube"></i></a></li>
        </ul>
      </div>
      <div class="col-md-4 text-center">
        <hr class="light">
        <h5>Contatti</h5>
        <hr class="light">
        <ul class="link-area">
          <li><a><i class="fas fa-phone"></i> 051 684 1930</a></li>
          <li><a><i class="fas fa-envelope"></i> AdminEpool@drive.com</a></li>
          <li><a href="https://www.unibo.it/it"><i class="fas fa-map-marked-alt"></i> Unibo</a></li>
          <li><a href="https://corsi.unibo.it/laurea/InformaticaManagement"><i class="fas fa-globe"></i> Informatica Per Il Management</a></li>
        </ul>
      </div>
      <div class="col-md-4 text-center">
        <hr class="light">
        <h5>Team</h5>
        <hr class="light">
        <ul class="link-area">
          <li><a > thomas.lodi@studio.unibo.it</a></li>
          <li><a > enrico.morselli@studio.unibo.it</a></li>
          <li><a > marco.aspromonte@studio.unibo.it</a></li>
        </ul>
      </div>
      <div class="col-12 text-center">
        <hr class="light">
        <h5>&copy; Unibo</h5>
      </div>
    </div>
  </div>
</footer>
