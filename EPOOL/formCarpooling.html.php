<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <title>Carpooling</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
      maximum-scale=1.0, minimum-scale=1.0">
      <!-- fogli di stile -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
    <!-- inserimento menu -->
    <?php include 'header.php'; ?>
    <div class="container" id="divContainer">
      <form action="cercaTratteDisponibili.php" method="post" id="dati">
        <div id="divR" align="center">
          <table class="table table-dark" style="background: rgba(0,0,0,0.5); width: 50%">
            <thead class="thead-dark">
              <tr align="center">
                  <th colspan="6" scope="col"><h3 class="text-center text-justify">Cerca Tragitto</h3></th>
              </tr>
            </thead>
            <tr>
              <td><h5 class="text-left font-weight-light">Partenza da:</h5></td>
              <td>
                <select class="form-control" name="IndirizzoPartenza" form="dati" required>
                  <option disabled selected value="">Seleziona indrizzo</option>
                  <?php
                    require 'db.inc.php';
                    try {
                      //query per recuperare gli indirizzi delle tappe
                      if ($_SESSION['tipo'] == "UD") {
                        $sql = 'CALL cercaTappeDip(?)';
                        $stmt = $pdo -> prepare($sql);
                        $stmt -> bindParam(1, $_SESSION['email']);
                      } else {
                        $sql = 'CALL cercaTappePrem()';
                        $stmt = $pdo -> prepare($sql);
                      }
                      $stmt -> execute();
                    } catch (Exception $e) {
                      echo "Errore nella query: ".$e -> getMessage();
                      exit();
                    }
                    //recupera il risultato della query
                    $res = $stmt -> fetchAll(PDO::FETCH_ASSOC);
                    //scorro il risultato
                    foreach ($res as $row): ?>
                  <option value="<?php echo $row['Via']; ?>">
                    <?php echo $row['Via']; ?>
                  </option>
                <?php endforeach; ?>
              </td>
            </tr>
          </table>
          <input type="submit" id="submit" name="action" value="CERCA TRAGITTO" class="btn btn-outline-light" style="background: rgba(0,0,0,0.5)">
        </div>
      </form>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <?php include "footer.php"; ?>

  </body>
</html>
