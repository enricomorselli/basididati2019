
<html>
    <link rel="stylesheet" media="(min-width: 640px)" href="min-640px.css">

    <head>
        <title>Epool homepage</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
          maximum-scale=1.0, minimum-scale=1.0">
          <!-- fogli di stile -->
          <link rel="stylesheet" href="css/bootstrap.min.css">
          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
          <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
    <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
        <!-- header -->
        <?php include 'header.php';?>
          <div class="container text-center" id="divContainer">
            <!-- pannello contenente testo, icone e pulsanti della pagina principale-->
            <div class="border border-white rounded"style="padding: 50px 10px; background: rgba(0,0,0,0.5);">
              <div class="row">
                  <div class="col">
                    <h1 class="text-center text-justify font-italic" style="margin-bottom: 5%; color: #fff;">I nostri servizi</h1>
                  </div>
              </div>
              <!--riga icone-->
              <div class="row">
                <div class="col">
                  <!--icona carsharing-->
                  <span class="fa-stack fa-4x">
                    <i class="fas fa-car fa-stack-2x text-light"></i>
                  </span>
                  <h4 class="service-heading" style="color: #fff;">Carsharing</h4>
                  <p class="text-center text-justify font-italic w-50 mx-auto" style="margin-bottom: 10%; color: #fff;">
                    Prenota un auto tra quelle disponibili e vai dove vuoi. Disponibile per tutte le categorie di utenti.
                  </p>
                </div>
                <div class="col">
                  <!--icona carpooling-->
                  <span class="fa-stack fa-4x">
                    <i class="fas fa-users fa-stack-2x text-light"></i>
                  </span>
                  <h4 class="service-heading" style="color: #fff;">Carpooling</h4>
                  <p class="text-center text-justify font-italic w-50 mx-auto" style="margin-bottom: 10%; color: #fff;">
                    Unisciti ad una prenotazione e condividi le spese. Disponibile solo per utenti premium o dipendenti.
                  </p>
                </div>
              </div>
              <?php if(isset($_SESSION['email'])) {?>
                <div class="row">
                  <div class="col">
                    <form action="" method="post">
                        <button class="btn btn-outline-light my-2 my-sm-0" style="width: 80%" type="submit" name="action" value="CARSHARING" id='btnCarsharing'>Vai al Carsharing</button>
                    </form>
                  </div>
                  <div class="col">
                    <form action="" method="post">
                        <button class="btn btn-outline-light my-2 my-sm-0" style="width: 80%" type="submit" name="action" value="CARPOOLING" id='btnCarpool'>Vai al Carpooling</button>
                    </form>
                  </div>
                </div>
                  <?php } else { ?>
                  <div class="row">
                    <div class="col">
                      <h3 class="text-center text-justify font-italic" style="color: #fff">Accedi o registrati per accedere ai nostri servizi</h3>
                  <?php  } ?>
                    <h5 class="text-center text-justify font-italic" style="margin-top:5%; color: #fff;">Noleggiare un' auto è ora più comodo e ecosostenibile!</h5>
                  </div>
              </div>
            </div>
          </div>
        <!--Footer della pagina-->
        <?php include "footer.php"; ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
