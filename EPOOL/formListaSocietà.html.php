<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Elenco Società</title>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
      maximum-scale=1.0, minimum-scale=1.0">
      <!-- fogli di stile -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
    <!--inserimento header-->
    <?php include 'header.php'; ?>
    <div class="container" id="divContainer">
      <table class="table table-dark" style="background: rgba(0,0,0,0.5); ">
        <thead class="thead-dark">
          <tr align="center">
            <!--header della tabella-->
            <th colspan="6" scope="col">SOCIETÀ DI CARSHARING</th>
          </tr>
          <tr>
            <!--header di ciascuna colonna-->
            <th scope="col">Codice</th>
            <th scope="col">Nome</th>
            <th scope="col">N° Veicoli</th>
            <th scope="col">Telefono</th>
            <th scope="col">Brochure</th>
            <th scope="col">Sito Web</th>
          </tr>
        </thead>
        <?php
        if (isset($resp)): ?>
          <tbody>
            <?php //scorre il risultato della query e riempie le celle della tabella
            foreach ($resp as $listaSoc): ?>
            <form method='post' action = "brochure.php" id='dati'>
            <tr>
              <td style="width: 5%"><?php echo $listaSoc['CodSocieta']; ?></td>
              <td style="width: 10%"><?php echo $listaSoc['Nome']; ?></td>
              <td style="width: 15%"><?php echo $listaSoc['NumVeicoli']; ?></td>
              <td style="width: 10%"><?php echo $listaSoc['Telefono']; ?></td>
              <td style="width: 10%"><input  type="hidden" value="<?php echo $listaSoc['Nome']; ?>" name="nome"> <input type="submit" name="action"  value="Vedi Brochure" class="btn btn-primary"  onclick="window.location='brochure.php';" /></td>

              <td style="width: 10%"><?php echo $listaSoc['URLsito']; ?></td>
            </tr>
            </form>
        <?php endforeach;
        else: ?>
            <tr align="center">
              <td colspan="5"><strong>NON CI SONO SOCIETÀ NEL DATABASE</strong></td>
            </tr>

        <?php endif; ?>
          </tbody>
        </table>
      </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <?php include "footer.php"; ?>

  </body>
</html>
