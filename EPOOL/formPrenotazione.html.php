<html>
<link rel="stylesheet" media="(min-width: 640px)" href="min-640px.css">

  <head>
    <title>Prenota Auto</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
      maximum-scale=1.0, minimum-scale=1.0">
      <!-- fogli di stile -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
  <!-- inserimento menu -->
  <?php include 'header.php'; ?>
  <div class="container" id="divContainer">
    <form method='post' action ="prenotazione.php" id='dati'>
    <div id="divR" align="center">
    <table class="table table-dark" style="background: rgba(0,0,0,0.5); width: 50%">
      <thead class="thead-dark">
        <tr align="center">
            <th colspan="2" scope="col"><h3 class="text-center text-justify">Nuova Prenotazione</h3></th>
        </tr>
      </thead>
      <tr>
          <td><h5 class="text-left font-weight-light">Modello:</h5></td>
          <td><input class="form-control" value="<?php echo $modello ?>" name="Modello" readonly></td>
      </tr>
      <tr>
          <td><h5 class="text-left font-weight-light">Targa veicolo:</h5></td>
          <td><input class="form-control" value="<?php echo $targa ?>" name="TargaVeicolo" readonly></td>
      </tr>
      <tr>
          <td><h5 class="text-left font-weight-light">Indirizzo partenza:</h5></td>
          <td><input class="form-control" value="<?php echo $indirizzoP ?>" name="IndirizzoPartenza" readonly></td>
      </tr>
        <tr>
            <td><h5 class="text-left font-weight-light">Note:</h5></td>
            <td><input class="form-control" type="text" maxlength="200" name="CampoNote"></td>
        </tr>
        <tr>
            <td><h5 class="text-left font-weight-light">Indirizzo arrivo:</h5></td>
            <!--<td><input type="text" maxlength="30" name="IndirizzoArrivo" required></td>-->
            <td>
              <select class="form-control" name="IndirizzoArrivo" form="dati" required>
                <option disabled selected value="">Seleziona indirizzo arrivo</option>
                <?php
                  require 'db.inc.php';
                  try {
                    //query per recuperare gli indirizzi delle aree di sosta
                    $sql = 'SELECT Indirizzo
                            FROM AREASOSTA
                            WHERE Indirizzo != "'.$indirizzoP.'"';
                    $stmt = $pdo -> prepare($sql);
                    $stmt -> execute();
                  } catch (Exception $e) {
                    echo "Errore nella query: ".$e;
                    exit();
                  }
                  //recupero il risultato della query
                  $res = $stmt -> fetchAll(PDO::FETCH_ASSOC);
                  //scorro il risultato
                  foreach ($res as $row): ?>
                  <option value="<?php echo $row['Indirizzo']; ?>">
                    <p><?php echo $row['Indirizzo']; ?></p>
                  </option>
                <?php endforeach; ?>
              </select>
            </td>
        </tr>
        <!-- riga checkbox "abilitaCarpool" -->
        <tr id="carpoolRow" style="display: none">
          <td><h5 class="text-left font-weight-light">Condividi tragitto</h5></td>
          <td><input class="form-check-input" type="checkbox" name="abilitaCarpool" id="abilitaCarpool" onclick="showForm()"></td>
        </tr>
    </table>
    <div id="inserimentoOrarioArrivo" style="display:none">
      <table class="table table-dark" style="background: rgba(0,0,0,0.5); width: 50%">
        <tr>
          <td><h5 class="text-left font-weight-light">Orario stimato di arrivo:</h5></td>
          <td><input class="form-control" type="time" id="inputOraArrivo" name="orarioArrivo"></td>
        </tr>
      </table>
    </div>
    <div id="inserimentoTappe" style="display:none">
      <table class="table table-dark" style="background: rgba(0,0,0,0.5); width: 50%">
        <tr>
          <td><h5 class="text-left font-weight-light">Aggiungi tappe intermedie:</h5></td>
          <td><button type="button" name="add" id="add" class="btn btn-outline-success">+</button></td>
        </tr>
      </table>
      <table class="table table-dark" style="background: rgba(0,0,0,0.5); width: 50%" id="dynamic_field">
      </table>
    </div>
    <input type ="submit" id="submit" name="action" value="PRENOTA VEICOLO" class="btn btn-lg btn-outline-light" style="background: rgba(0,0,0,0.5)">
    </div>
    </form>
  </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript">

    //funzione per mostrare il form per il carpooling
      function showForm() {
        //ottieni la checkbox
        var checkbox = document.getElementById('abilitaCarpool');
        //ottieni il div per l'inserimento tappe
        var divTappe = document.getElementById('inserimentoTappe');
        //div per inserire l'orario di arrivo stimato
        var divOrario = document.getElementById('inserimentoOrarioArrivo');
        //input per l'orario d'arrivo stimato
        var inputOrario = document.getElementById('inputOraArrivo');
        //se la checkbox viene selezionata, mostra il div per l'inserimento delle tappe
        if (checkbox.checked == true) {
          divTappe.style.display = "block";
          divOrario.style.display = "block";
          inputOrario.required = true;
        } else {
          divTappe.style.display = "none";
          divOrario.style.display = "none";
          inputOrario.required = false;
        }
      }

      $(document).ready(function(){
        var i = 1;
        //passo la variabile di sessione 'tipo' a jquery
        var tipoUtente = <?php echo json_encode($_SESSION['tipo']) ?>;
        var row = document.getElementById('carpoolRow');
        //mostra la checkbox per abilitare il carpooling solo ad utenti
        //premium o dipendenti
        if (tipoUtente == 'UD' || tipoUtente == 'UP') {
          row.style.display = "block";
        } else {
          row.style.display = "none";
        }
        //aggiungi tappa
        $('#add').click(function(){
          i++;
          $('#dynamic_field')
          .append('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="tappa[]" placeholder="Inserisci indirizzo tappa" class="form-control name_list" required /></td><td><input type="time" name="oraStimata[]" class="form-control name_list" required /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-outline-danger btn_remove">X</button></td></tr>');
        });
        //rimuovi tappa
        $(document).on('click', '.btn_remove', function(){
          var button_id = $(this).attr("id");
          $('#row'+button_id+'').remove();
        });
      });
    </script>
    <?php include 'footer.php'; ?>

  </body>
  </html>
