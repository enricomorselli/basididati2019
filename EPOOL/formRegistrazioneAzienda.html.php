<html>
  <head>
    <title>Registrazione Azienda</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
      maximum-scale=1.0, minimum-scale=1.0">
      <!-- fogli di stile -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
    <!-- inseriemnto menu -->
    <?php include 'header.php'; ?>
    <div class="container" id="divContainer">
      <form method='post' action = "registrazioneAzienda.php" id='dati'>
      <div id="divR" align="center">
      <table class="table table-dark" style="background: rgba(0,0,0,0.5); width: 50%">
        <thead class="thead-dark">
          <tr align="center">
            <th colspan="2" scope="col"><h3 class="text-center text-justify">Registrazione Azienda</h3></th>
          </tr>
        </thead>
          <tr>
              <td><h5 class="text-left text-light font-weight-light">Nome: </h5></td>
              <td><input  class="form-control mr-sm-2" type="text" maxlength="50" name="NomeAzienda" placeholder="Nome Azienda..." required></td>
          </tr>
          <tr>
              <td><h5 class="text-left text-light font-weight-light">Indirizzo: </h5></td>
              <td><input  class="form-control mr-sm-2" type="text" maxlength="50" name="Indirizzo" placeholder="Indirizzo..." required></td>
          </tr>
          <tr>
              <td><h5 class="text-left text-light font-weight-light">Telefono: </h5></td>
              <td><input  class="form-control mr-sm-2" type="long" maxlength="200" name="Telefono" placeholder="Telefono..." required></td>
          </tr>
          <tr>
              <td><h5 class="text-left text-light font-weight-light">Telefono responsabile: </h5></td>
              <td><input  class="form-control mr-sm-2" type="long" maxlength="200" name="TelefonoResponsabile" placeholder="Telefono responsabile..." required></td>
          </tr>
      </table>
      <input type ="submit" class="btn btn-lg btn-outline-light" style="background: rgba(0,0,0,0.5)" name="action" value="REGISTRA AZIENDA" class="btn btn-lg btn-primary">
      </div>
      </form>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <?php include "footer.php"; ?>

  </body>
  </html>
