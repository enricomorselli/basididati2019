<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>storico carpooling</title>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
      maximum-scale=1.0, minimum-scale=1.0">
      <!-- fogli di stile -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
    <!--inserimento header-->
    <?php include 'header.php'; ?>
    <div class="container" id="divContainer">
      <table class="table table-dark" style="background: rgba(0,0,0,0.5); ">
        <thead class="thead-dark">
          <tr align="center">
            <!--header della tabella-->
            <th colspan="4" scope="col"><h3 class="text-center text-justify">Tragitti condivisi</h3></th>
          </tr>
          <tr>
            <!--header di ciascuna colonna-->
            <th scope="col">#</th>
            <th scope="col">Indirizzo Partenza</th>
            <th scope="col">Indirizzo Arrivo</th>
            <th scope="col">Compagni di viaggio</th>
        </thead>
        <?php
        //variabile per la colonna #
        $numeroTragitto = 0;
        if (isset($resp)): ?>
          <tbody>
            <?php //scorre il risultato della query e riempie le celle della tabella
            foreach ($resp as $trag): ?>
            <tr>
              <th scope="row" style="width: 5%"><?php echo $numeroTragitto + 1;
              $numeroTragitto = $numeroTragitto + 1; ?></th>
              <td style="width: 10%"><?php echo $trag['TappaPartenza']; ?></td>
              <td style="width: 10%"><?php echo $trag['TappaArrivo']; ?></td>
              <td style="width: 10%">
                <form action="visualizzaUtentiTragitto.php" method="post">
                  <input type="hidden" name="CodTragitto"
                      value="<?php echo ($trag['CodTragitto']); ?>">
                  <input type="submit" name="action" value="VISUALIZZA"
                      class="btn btn-outline-light" id='btnUtentiTragitto'>
                </form>
              </td>
            </tr>
        <?php endforeach;
        else: ?>
            <tr align="center">
              <td colspan="5"><strong>NESSUNA PRENOTAZIONE</strong></td>
            </tr>
        <?php endif; ?>
          </tbody>
        </table>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <?php include "footer.php"; ?>

  </body>
</html>
