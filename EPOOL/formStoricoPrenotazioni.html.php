<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Riepilogo prenotazioni</title>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
      maximum-scale=1.0, minimum-scale=1.0">
      <!-- fogli di stile -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
    <!--inserimento header-->
    <?php include 'header.php'; ?>
    <div class="container" id="divContainer">
      <table class="table table-dark" style="background: rgba(0,0,0,0.5);">
        <thead class="thead-dark">
          <tr align="center">
            <!--header della tabella-->
            <th colspan="10" scope="col"><h3 class="text-center text-justify">Prenotazioni effettuate</h3></th>
          </tr>
          <tr>
            <!--header di ciascuna colonna-->
            <th scope="col">#</th>
            <th scope="col">Note</th>
            <th scope="col">Orario Inizio</th>
            <th scope="col">Orario Fine</th>
            <th scope="col">Indirizzo Partenza</th>
            <th scope="col">Indirizzo Arrivo</th>
            <th scope="col">Targa Veicolo</th>
            <th scope="col">Durata (min)</th>
            <th scope="col">Costo</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <?php
        //variabile per la colonna #
        $numeroPrenotazioni = 0;
        if (isset($resp)): ?>
          <tbody>
            <?php //scorre il risultato della query e riempie le celle della tabella
            foreach ($resp as $prenotazione): ?>
            <tr>
              <th scope="row" style="width: 5%"><?php echo $numeroPrenotazioni + 1;
              $numeroPrenotazioni = $numeroPrenotazioni + 1; ?></th>
              <td style="width: 10%"><?php echo $prenotazione['CampoNote']; ?></td>
              <td style="width: 10%"><?php echo $prenotazione['OraInizio']; ?></td>
              <td style="width: 10%"><?php echo $prenotazione['OraFine']; ?></td>
              <td style="width: 10%"><?php echo $prenotazione['IndirizzoPartenza']; ?></td>
              <td style="width: 10%"><?php echo $prenotazione['IndirizzoArrivo']; ?></td>
              <td style="width: 10%"><?php echo $prenotazione['TargaVeicolo']; ?></td>
              <td style="width: 10%"><?php echo $prenotazione['Durata']; ?></td>
              <td style="width: 10%"><?php echo $prenotazione['Costo']; ?></td>
              <td style="width: 10%">
                <?php
                  if (is_null($prenotazione['OraFine'])): ?>
                <form action="terminaPrenotazione.php" method="post">
                      <input type="hidden" name="TargaVeicolo" value="<?php echo $prenotazione['TargaVeicolo']; ?>">
                      <input type="hidden" name="IndirizzoArrivo" value="<?php echo $prenotazione['IndirizzoArrivo']; ?>">
                      <input type="submit" name="action" value="TERMINA" class="btn btn-outline-light" id='btnTermina' style="margin: 5px">
                </form>
              <?php else: ?>
                <p>Conclusa</p>
              <?php endif; ?>
              <!-- bottone per inserire una segnalazione sullo stato di un veicolo -->
              <form action="formSegnalaVeicolo.html.php" method="post">
                    <input type="hidden" name="TargaVeicolo" value=<?php echo $prenotazione['TargaVeicolo']; ?>>
                    <input type="submit" name="action" value="SEGNALA" class="btn btn-outline-light" id='btnTermina' style="margin: 5px">
              </form>
              </td>
            </tr>
        <?php endforeach;
        else: ?>
            <tr align="center">
              <td colspan="5"><strong>NON CI SONO PRENOTAZIONI NEL DATABASE</strong></td>
            </tr>
        <?php endif; ?>
          </tbody>
        </table>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <?php include "footer.php"; ?>

  </body>
</html>
