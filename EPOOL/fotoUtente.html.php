<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Utenti attivi</title>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
      maximum-scale=1.0, minimum-scale=1.0">
      <!-- fogli di stile -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
      </head>
      <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
           <?php
           require 'mongodb.inc.php';
           include 'header.php';
           $EmailUtente =$_POST['email'];
           ?>
        <form action="listaUtenti.php" method="POST">
            <input type="hidden" name="EmailUtente" value=<?php echo $EmailUtente; ?>>
        </form>

        <div class="container" id="divContainer" align="center">
          <h3 class="text-center text-justify text-light"  style="background: rgba(0,0,0,0.5); width:70%">Foto dell'utente <?php echo $EmailUtente; ?> :</h3>
            <table style="background: rgba(0,0,0,0.5); width:70%">
                   <?php
                   // Connessione al DB
                   require 'db.inc.php';
                   try {

                      $query = "CALL stampaFoto(?)";
                      $stmt = $pdo->prepare($query);
                      $stmt->bindParam(1, $EmailUtente);
                      $stmt->execute();
                      $ris = $stmt->fetchAll();
                      echo "<tr>";
                      $i = 2;
                      foreach ($ris as $ris2) {
                         $url = "image/" . $ris2['Immagine'];
                         echo '<td><img src="'. $url .'" class="img-fluid" alt="Responsive image"></td>';
                         if ($i % 2 != 0) {
                            echo "</tr><tr>";
                         }
                         $i++;
                      }
                      if ($i==2) echo "<td><h3 class='text-center text-justify font-weight-light text-light font-italic'>Non ha ancora caricato foto</h3></td>";
                      echo "</tr>";
                   } catch (Exception $ex) {
                      $ex->getMessage();
                      exit();
                   }
                   ?>

                </table>
            </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
    <?php include "footer.php"; ?>
</html>
