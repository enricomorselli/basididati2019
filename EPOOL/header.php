<?php ob_start(); ?>

<?php
//connessione al database mongodb
require 'mongodb.inc.php';
//Controllo se l'utente è loggato
function is_loggedin() {
   $status = session_status();
   if ($status == PHP_SESSION_NONE) {
      session_start();
   }
   if (isset($_SESSION['email'])) {
      return true;
   } else {
      return FALSE;
   }
}
//Se l'utente è loggato posso visualizzare le funzionalità adatte per quell'utente
if (is_loggedin()) {

?>

   <form action="" method="post">
       <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
           <a class="navbar-brand" href="index.php">E-pool</a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
           </button>
           <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
               <div class="navbar-nav">
                   <a class="nav-item nav-link " href="index.php">Home<span class="sr-only">(current)</span></a>
                   <li class="nav-item dropdown">
                       <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           Messaggi
                       </a>
                       <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                           <a class="dropdown-item" href="messaggi.php">Invia messaggio</a>
                           <div class="dropdown-divider"></div>
                           <a class="dropdown-item" href="postaArrivo.php">Posta in arrivo</a>
                       </div>
                   </li>
                   <li class="nav-item dropdown">
                       <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           Operazioni
                       </a>
                       <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                         <a class="dropdown-item" href="listaUtenti.php">Visualizza Utenti<span class="sr-only">(current)</span><input type="hidden" name="listaUtenti"></a>
                         <a class="dropdown-item" href="storicoPrenotazioni.php">Storico Prenotazioni</a>
                         <a class="dropdown-item" href="listaSocieta.php">Elenco Società</a>
                          <!--se l'utente è di tipo PREMIUM ha a disposizione le seguenti operazioni aggiuntive-->
                           <?php if ($_SESSION['tipo'] == "UP" || $_SESSION['tipo'] == "UD"): ?>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="storicoCarpooling.php">Storico Tragitti</a>
                           <?php endif; ?>
                       </div>
                   </li>
                   <li class="nav-item dropdown">
                       <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           Statistiche
                       </a>
                       <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                           <a class="dropdown-item" href="classificaUtenti.php">Classifica utenti</a>
                           <div class="dropdown-divider"></div>
                           <a class="dropdown-item" href="utentiAttivi.php">Utenti attivi</a>
                           <div class="dropdown-divider"></div>
                           <a class="dropdown-item" href="classificaAuto.php">Veicoli più prenotati</a>
                       </div>
                   </li>
               </div>
               <div class="navbar-nav ml-auto">
                   <li class="nav-item dropdown">
                       <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           Utente
                       </a>
                       <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                           <a class="dropdown-item" href="profiloUtente.html.php"><label id="label"><?php echo $_SESSION['email'] ?></label></a>
                           <div class="dropdown-divider"></div>
                           <a class="dropdown-item"><label id="label">Tipo: <?php echo $_SESSION['tipo'] ?></label></a>
                           <div class="dropdown-divider"></div>
                           <a class="dropdown-item" href="formUpload.html.php">Carica Foto</a>

                       </div>
                   </li>
                   <a class="nav-item nav-link " href="logout.php">Logout<span class="sr-only">(current)</span></a>
               </div>
           </div>
       </nav>
   </form>

    <?php
    } else {
    ?>
          <!--Navbar mostrata quando l'utente non è loggato-->
           <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="index.php">E-pool</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                   <span class="navbar-toggler-icon"></span>
                </button>
                <!-- Navbar collapse -->
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Home<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="formRegistrazioneUtente.html.php">Registrati</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="formRegistrazioneAzienda.html.php">Registra Azienda</a>
                    </li>
                  </ul>
                  <form class="form-inline my-2 my-lg-0" action="login.php" method="post">
                    <input class="form-control mr-sm-2" type="text" name="email" placeholder="Email..." aria-label="Email" required>
                    <input class="form-control mr-sm-2" type="password" name="password" placeholder="Password..." aria-label="Password" required>
                    <button class="btn btn-outline-light my-2 my-sm-0" type="submit" name="action" value="ACCEDI">Login</button>
                  </form>
               </div>
           </nav>
   <?php
}
?>
