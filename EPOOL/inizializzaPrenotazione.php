<?php
//connessione al database e mongodb
require 'mongodb.inc.php';
require 'db.inc.php';

try {
  //targa del veicolo selezionato dall'utente
  $targa = $_POST['TargaVeicolo'];
  $query = 'CALL selectAuto(?)';
  $stmt = $pdo -> prepare($query);
  $stmt -> bindParam(1, $targa);
  if (!$stmt -> execute()) {
    echo "Errore nella query: ".$dbc -> error.".";
  } else {
    //auto selezionata per la prenotazione
    $resp = $stmt -> fetchAll(PDO::FETCH_ASSOC);
  }
} catch (Exception $e) {
  echo "Errore: ".$e;
  exit();
}

//variabili relative al veicolo da prenotare
  foreach ($resp as $autoSel) {
    $modello = $autoSel['Modello'];
    $indirizzoP = $autoSel['Indirizzo'];
  }

  include 'formPrenotazione.html.php';
