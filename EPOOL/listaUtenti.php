<?php
//connessione al database mongodb
require 'mongodb.inc.php';
//script per la visualizzazione della lista degli utenti registrati
//connessione al db
require 'db.inc.php';
//funzione per creare finestra di alert javascript
function alert($msg) {
   echo "<script type='text/javascript'>alert('$msg');</script>";
}
//richiamiamo la stored procedure per ottenere gli utenti registrati
try {
  //recupera la mail dell'utente attualmente loggato
  session_start();
  $utente = $_SESSION['email'];
  //chiamata alla stored procedure
  $query = 'CALL VisualizzaUtenteDati(?)';
  $stmt = $pdo -> prepare($query);
  $stmt -> bindParam(1, $utente);
  if (!$stmt -> execute()) {
    echo "Errore nella query " . $dbc -> error. ".";
  } else {
    $resp = $stmt -> fetchAll(PDO::FETCH_ASSOC);
  }
} catch (Exception $e) {
  echo "Errore: " . $e . ".";
  exit();
}
//include il form per mostrare gli utenti presenti nel db
include 'formListaUtenti.html.php';
