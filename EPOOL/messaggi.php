<?php
//connessione al database mongodb
require 'mongodb.inc.php';

function alert($msg) {
    echo "<script type='text/javascript'>alert('$msg');</script>";
}

if(isset($_POST['action']) and $_POST['action'] == 'Invia messaggio'){
  //connessione al db
  require 'db.inc.php';
  //recupero dati del form
   try{
      session_start();
      $emailMittente = $_SESSION['email'];
      $emailDestinatario = $_POST['emailDestinatario'];
      $titolo = $_POST['titolo'];
      $testo = $_POST['testo'];
      $data = date("y/m/d");
      $query = "CALL invioMessaggio(?,?,?,?,?,@out)";
      $stmt = $pdo->prepare($query);
      $stmt->bindParam(1, $titolo);
      $stmt->bindParam(2, $emailMittente);
      $stmt->bindParam(3, $emailDestinatario);
      $stmt->bindParam(4, $testo);
      $stmt->bindParam(5, $data);
      if(!$stmt->execute()){
         echo "Errore della query: " .$dbc->error . ".";
      }else{
        $output = $pdo->query("select @out;")->fetch();
        $num = $output['@out'];
         if($num == 1){
            alert('Messaggio inviato correttamente!');
            header( "refresh:1;url=postaArrivo.php" );
         }else{
            alert('Utente destinatario non presente nel database');
         }
      }
   }catch(Exception $ex){
      echo $ex->getMessage();
      exit();
   }
}
if (isset($_SESSION['email'])) {
$data=date("Y-m-d H:i:s");
$bulkWrite = new MongoDB\Driver\BulkWrite;
$doc = ['avviso' => 'invio messaggio',
  'utente mittente' => $_SESSION['email'],'email destinatario' => $emailDestinatario , 'data' => $data ];
$bulkWrite->insert($doc);
$manager->executeBulkWrite('epool.logEpool', $bulkWrite);
}

include 'formMessaggi.html.php';
