<?php
//connessione al database mongodb
require 'mongodb.inc.php';

function alert($msg) {
   echo "<script type='text/javascript'>alert('$msg');</script>";
}

if (isset($_POST['action']) and $_POST['action'] == "SELEZIONA ARRIVO") {

  require 'db.inc.php';

  session_start();
  $email = $_SESSION['email'];
  $tipo = $_SESSION['tipo'];
  $codTragitto = $_POST['CodTragitto'];
  $partenza = $_POST['IndirizzoPartenza'];
  //indirizzo di arrivo selezionato dall'utente
  $arrivo = $_POST['IndirizzoArrivo'];

  try {
    if ($tipo == "UP") {
      $sql = 'CALL prenotaTragittoPrem(?, ?, ?, ?)';
    } elseif ($tipo == "UD") {
      $sql = 'CALL prenotaTragittoDip(?, ?, ?, ?)';
    }
    $stmt = $pdo -> prepare($sql);
    $stmt -> bindParam(1, $codTragitto);
    $stmt -> bindParam(2, $email);
    $stmt -> bindParam(3, $partenza);
    $stmt -> bindParam(4, $arrivo);
    $stmt -> execute();

  } catch (Exception $e) {
    echo "Errore nella prenotazione: ".$e -> getMessage();
    exit();
  }

  alert("Tragitto prenotato con successo");
  header("Location: storicoCarpooling.php");

} else {
  header("Location: index.php");
  exit();
}
if (isset($_SESSION['email'])) {
$data=date("Y-m-d H:i:s");
$bulkWrite = new MongoDB\Driver\BulkWrite;
$doc = ['avviso' => 'prenotazione tragitto',
  'utente' => $_SESSION['email'], 'data' => $data , 'codice tragitto'=>$codTragitto,
  'indirizzo partenza' => $partenza, 'indirizzo arrivo' => $arrivo, 'codice prenotazione' => $codTragitto ];
$bulkWrite->insert($doc);
$manager->executeBulkWrite('epool.logEpool', $bulkWrite);
}
