<!DOCTYPE html PUBLIC>
<html>
  <head>
    <link rel="stylesheet" href="css/bootstrap.min.css" >
  </head>
<body>
<?php
//connessione al database mongodb
require 'mongodb.inc.php';
require 'db.inc.php';
include 'header.php';

  //funzione per creare finestra di alert javascript
  function alert($msg) {
     echo "<script type='text/javascript'>alert('$msg');</script>";
  }

if(isset($_POST['action']) and $_POST['action'] == 'PRENOTA VEICOLO'){
  //inizializza variabili prenotazione
  //variabili prese dal form
  $CampoNote = $_POST['CampoNote'];
  $IndirizzoPartenza = $_POST['IndirizzoPartenza'];
  $IndirizzoArrivo = $_POST['IndirizzoArrivo'];
  $OrarioArrivo = $_POST['orarioArrivo'];
  //email utente attualmente loggato
  $Email = $_SESSION['email'];
  $TargaVeicolo = $_POST['TargaVeicolo'];

  //Se il carpooling non è stato abilitato ...
  if (!isset($_POST['abilitaCarpool'])){

    try {
      //procedure per inserire nuova prenotazione
      $query="CALL NuovaPrenotazione(?,?,?,?,?)";
      $stmt = $pdo->prepare($query);
      $stmt->bindParam(1,$CampoNote);
      $stmt->bindParam(2,$IndirizzoPartenza);
      $stmt->bindParam(3,$IndirizzoArrivo);
      $stmt->bindParam(4,$Email);
      $stmt->bindParam(5,$TargaVeicolo);

      $stmt->execute();
      $stmt->closeCursor();

      include 'cordinate.php';

    } catch (Exception $e) {
      echo "Errore nella procedure NuovaPrenotazione: ".$e.".";
      exit();
    }

  } else {

    try {
      //inserimento di una prenotazione con carpooling abilitato
      $sql = 'CALL NuovaPrenotazioneCarpooling(?,?,?,?,?)';
      $stmt = $pdo -> prepare($sql);
      $stmt->bindParam(1,$CampoNote);
      $stmt->bindParam(2,$IndirizzoPartenza);
      $stmt->bindParam(3,$IndirizzoArrivo);
      $stmt->bindParam(4,$Email);
      $stmt->bindParam(5,$TargaVeicolo);

      $stmt->execute();
      $stmt->closeCursor();

    } catch (Exception $e) {
      echo "Errore nell'inserimento del tragitto: ".$e;
      exit();
    }

    //array contenente le tappe del tragitto. La prima tappa sarà l'Indirizzo
    //di partenza della prenotazione
    $tappe = array();
    $orari = array();
    //inserisco indirizzo di partenza in $tappe
    //array_push($tappe, $IndirizzoPartenza);
    $tappe[0] = $IndirizzoPartenza;
    //inserisco orario di inizio prenotazione in $orari (Ora:minuti:secondi)
    $orari[0] = date("H:i:s");

    //se l'utente ha inserito ulteriori tappe intermedie...
    if (!empty($_POST['tappa']) and !empty($_POST['oraStimata'])) {
      $i = 1;
      //scorro le tappe inserite dall'utente
      foreach ($_POST['tappa'] as $key => $value) {
        //inserisco l'indrizzo della tappa in $tappe
        $tappe[$i] = $value;
        $i++;
      }
      $j = 1;
      //scorro gli orari stimati inseriti dall'utente
      foreach ($_POST['oraStimata'] as $key => $value) {
        $orari[$j] = $value;
        $j++;
      }
    //inserisco l'indirizzo di arrivo in $tappe
    $tappe[$i] = $IndirizzoArrivo;
    //inserisco orario stimato di arrivo in $orari
    $orari[$j] = $OrarioArrivo;

    } else {
      $tappe[1] = $IndirizzoArrivo;
      $orari[1] = $OrarioArrivo;
    }


    //inserimento delle tappe nel db
    for ($i = 0; $i < count($tappe); $i++) {
      try {
      $sql = 'CALL inserisciTappa(?,?,?)';
      $stmt = $pdo -> prepare($sql);
      $stmt -> bindParam(1, $tappe[$i]);
      $stmt -> bindParam(2, $i);
      $stmt -> bindParam(3, $orari[$i]);

      $stmt -> execute();

      } catch (Exception $e) {
        echo "Errore nell'inserimento tappa n ".$i." : ".$e -> getMessage();
        exit();
      }

    }
  }

    //Aggiornamento tipo utente post prenotazione
    try {

      $tipo = $_SESSION['tipo'];

      if ($tipo == "US") {

        $query = "CALL checkTipoUtente(?, @out)";
        $stmt = $pdo -> prepare($query);
        $stmt -> bindParam(1, $Email);
        $stmt -> execute();
        $output = $pdo -> query("SELECT @out;") -> fetch();
        $num = $output['@out'];

          if($num == 1){
            $_SESSION['tipo'] = "UP";
            echo "<script>alert('Hai effettuato 3 prenotazioni con noi, ora sei un utente PREMIUM!');</script>";
          }
        }

      }  catch (Exception $e) {
          echo "Errore nella procedure checkTipoUtente: ".$e -> getMessage().".";
          exit();
      }
    //in caso di successo
    ?>
    <!--<h3>prenotazione avveuta con successo!</h3>-->
    <script>window.location = './storicoPrenotazioni.php',2000;</script>
    <?php

  } else {
    //se l'utente non ha cliccato sul pulsante "PRENOTAVEICOLO"
    header("Location: index.php");
    exit();
  }

  if (isset($_SESSION['email'])) {
    $data=date("Y-m-d H:i:s");
    $bulkWrite = new MongoDB\Driver\BulkWrite;
    $doc = ['avviso' => 'effettuata prenotazione',
      'utente' => $_SESSION['email'], 'data' => $data , 'Targa' => $TargaVeicolo];
    $bulkWrite->insert($doc);
    $manager->executeBulkWrite('epool.logEpool', $bulkWrite);
  }

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/bootstrap.min.js" ></script>
</body>
</html>
