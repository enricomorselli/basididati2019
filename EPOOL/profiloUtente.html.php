<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Profilo Utente</title>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
      maximum-scale=1.0, minimum-scale=1.0">
      <!-- fogli di stile -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">

           <?php
           include 'header.php';
           require 'db.inc.php';

           try {
             //recupera la mail dell'utente attualmente loggato

             $EmailUtente = $_SESSION['email'];
             //chiamata alla stored procedure
             $query = 'CALL VisualizzaDatiUtenteLog(?)';
             $stmt = $pdo -> prepare($query);
             $stmt -> bindParam(1, $EmailUtente);
             if (!$stmt -> execute()) {
               echo "Errore nella query " . $dbc -> error. ".";
             } else {
               $resp = $stmt -> fetchAll(PDO::FETCH_ASSOC);
               $stmt->closeCursor();
             }
           } catch (Exception $e) {
             echo "Errore: " . $e . ".";
             exit();
           }

            //scorre il risultato della query e riempie le celle della tabella
            if (isset($resp)):
           foreach ($resp as $utente):


           ?>
    <!-- Informazioni dell'utente -->
    <div class="container" id="divContainer" align="center">
      <h3 class="text-center text-justify text-light" style="background: rgba(0,0,0,0.5); width:50%">Informazioni utente</h3>
        <table class="table table-dark" style="background: rgba(0,0,0,0.5); width: 50%">
          <tr>
            <td><h5 class="text-left font-weight-light">Nome:</h5></td>
            <td><h5 class="text-left font-weight-light"><?php echo $utente['Nome']; ?></h5></td>
          </tr>
          <tr>
            <td><h5 class="text-left font-weight-light">Cognome:</h5></td>
            <td><h5 class="text-left font-weight-light"><?php echo $utente['Cognome']; ?></h5></td>
          </tr>
          <tr>
            <td><h5 class="text-left font-weight-light">Email:</h5></td>
            <td><h5 class="text-left font-weight-light"><?php echo $utente['Email']; ?></h5></td>
          </tr>
          <tr>
            <td><h5 class="text-left font-weight-light">Data nascita:</h5></td>
            <td><h5 class="text-left font-weight-light"><?php echo $utente['dataNascita']; ?></h5></td>
          </tr>
          <tr>
            <td><h5 class="text-left font-weight-light">Luogo nascita:</h5></td>
            <td><h5 class="text-left font-weight-light"><?php echo $utente['luogoNascita']; ?></h5></td>
          </tr>
        <?php endforeach;
      else: ?>
          <tr align="center">
            <td colspan="5"><h3 class="text-center text-justify text-light">Errore nei dati del DATABASE</h3></td>
          </tr>
        <?php endif; ?>
      </table>
      <?php
          //recupero delle valutazioni che vengono effettuate da parte di altri utenti
          $query2 = "CALL ValutazioniUtente(?)";
          $stmt2 = $pdo->prepare($query2);
          $stmt2->bindParam(1, $EmailUtente);
          if (!$stmt2->execute()) {
           echo "Errore della query: " . $pdo->error . ".";
          } else {
           $resp2 = $stmt2->fetchAll();
           $stmt2->closeCursor();
          }

          foreach ($resp2 as $row) {
              $valutazioneUtente[] = array('utenteValutatore' => $row['UtenteValutatore'], 'data' => $row["DataTesto"], 'voto' => $row['Voto'],
              'testo' => $row['Testo']);
          }

        ?>

        <!-- valutazioni ricevute dall'utente -->
        <div style="padding-top:100px">
            <!-- Fluid width widget -->
            <h3 class="text-center text-justify text-light" style="background: rgba(0,0,0,0.5); width:50%">Valutazioni Utente</h3>
            <div class="text-left card" style="background: rgba(0,0,0,0.5);">
              <div class="card-body">
                <ul class="list-group list-group-flush">
                  <?php if (isset($valutazioneUtente)):
                    foreach ($valutazioneUtente as $valutazione): ?>
                    <li class="list-group-item">
                      <h4 class="card-title"><?php echo($valutazione['utenteValutatore']); ?></h4>
                      <p class="float-sm-right"><?php echo($valutazione['data']); ?></p>
                      <p><?php echo($valutazione['testo']); ?></p>
                      <span class="badge" style="background-color: lightgrey">VOTO: <?php echo($valutazione['voto']); ?></span>
                    </li>
                    <?php endforeach; ?>
                  </ul>
            <?php else: ?>
               <h3 class="text-center text-justify font-weight-light text-light font-italic">Non hai ricevuto valutazioni</h3>
            <?php endif; ?>
              </div>
            </div>
          </div>

    <!-- Foto caricate dall'utente -->
    <div style="padding-top: 100px" align ="center">
      <h3 class="text-center text-justify text-light"  style="background: rgba(0,0,0,0.5); width:70%">Foto dell'utente</h3>
        <table style="background: rgba(0,0,0,0.5); width:70%">
               <?php
               // Connessione al DB
               require 'db.inc.php';
               try {

                  $query = "CALL stampaFoto(?)";
                  $stmt = $pdo->prepare($query);
                  $stmt->bindParam(1, $EmailUtente);
                  $stmt->execute();
                  $ris = $stmt->fetchAll();
                  echo "<tr>";
                  $i = 2;
                  foreach ($ris as $ris2) {
                     $url = "image/" . $ris2['Immagine'];
                     echo '<td><img src="'. $url .'" class="img-fluid" alt="Responsive image"></td>';
                     if ($i % 2 != 0) {
                        echo "</tr><tr>";
                     }
                     $i++;
                  }
                  if ($i==2) echo "<td><h3 class='text-center text-justify font-weight-light text-light font-italic'>Non hai ancora caricato foto</h3></td>";
                  echo "</tr>";
               } catch (Exception $ex) {
                  $ex->getMessage();
                  exit();
               }
               ?>

            </table>
        </div>
      </div>
    </body>
    <?php include "footer.php"; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</html>
