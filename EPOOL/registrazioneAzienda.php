<!DOCTYPE html PUBLIC>
<html>
  <head>
    <link rel="stylesheet" href="css/bootstrap.min.css" >
  </head>
  <body>
    <p>
    <?php
    //connessione al database mongodb
    require 'mongodb.inc.php';
    include 'header.php';
    if (isset($_POST['action']) and $_POST['action'] == 'REGISTRA AZIENDA') {

      //connessione al db
      require 'db.inc.php';
      //recupero dati del form
    try{
          $NomeAzienda = filter_input(INPUT_POST, 'NomeAzienda');
          $Indirizzo = filter_input(INPUT_POST, 'Indirizzo');
          $Telefono  = filter_input(INPUT_POST, 'Telefono' );
          $TelefonoResponsabile = filter_input(INPUT_POST, 'TelefonoResponsabile');

          $sql = 'SELECT  COUNT(*) as counter
                FROM AZIENDA
                WHERE Nome="'.$NomeAzienda.'"';

          $res = $pdo -> query($sql);
          $row = $res -> fetch();

          if ($row['counter'] == 0) {

          $query="CALL NuovaAzienda(?,?,?,?)";
          $stmt = $pdo->prepare($query);
          $stmt->bindParam(1,$NomeAzienda);
          $stmt->bindParam(2,$Indirizzo);
          $stmt->bindParam(3,$Telefono);
          $stmt->bindParam(4,$TelefonoResponsabile);

          $stmt->execute();
          $stmt->closeCursor();

          //registrazione azienda completata con successo
          echo "<script>alert('Registrazione azienda avvenuta con successo'); window.location = './index.php';</script>";

          if (isset($_SESSION['email'])) {
            $data=date("Y-m-d H:i:s");
            $bulkWrite = new MongoDB\Driver\BulkWrite;
            $doc = ['avviso' => 'registrazione nuova azienda',
              'azienda' => $NomeAzienda, 'data' => $data ];
            $bulkWrite->insert($doc);
            $manager->executeBulkWrite('epool.logEpool', $bulkWrite);
          }

        } else { //inserita NomeAzienda duplicata
          echo "<script>alert('Azienda gia esistente!'); window.location = './formRegistrazioneAzienda.html.php';</script>";
        }
      } catch(PDOException $e) {
        echo("[ERRORE] Esecuzione procedura non riuscita: ".$e->getMessage());
        exit();
      }

    } else {
      /* Se l'utente non ha cliccato il bottone "REGISTRATI" in
      formRegistrazioneAzienda.html.php allora non può accedere allo script
      registrazioneAzienda.php, e viene direzionato a
      formRegistrazioneAzienda.html.php */
      header("Location: formRegistrazioneAzienda.html.php");
      exit();
    }

    ?>
    </p>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js" ></script>
  </body>
</html>
