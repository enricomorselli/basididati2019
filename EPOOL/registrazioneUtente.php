<!DOCTYPE html PUBLIC>
<html>
  <head>
    <title>Registrazione Utente</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
      maximum-scale=1.0, minimum-scale=1.0">
      <!-- fogli di stile -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body>
<?php
//connessione al database mongodb
require 'mongodb.inc.php';

function alert($msg) {
   echo "<script type='text/javascript'>alert('$msg');</script>";
}

if (isset($_POST['action']) and $_POST['action'] == 'REGISTRA UTENTE') {
  // Connessione al DB
  require 'db.inc.php';
  //recupero dati del form
try {
      $Email  = filter_input(INPUT_POST, 'Email');
      $PasswordEm = filter_input(INPUT_POST, 'PasswordEm');
      $Password2 = filter_input(INPUT_POST, 'Password2');
      $DataNascita  = filter_input(INPUT_POST, 'DataNascita' );
      $LuogoNascita = filter_input(INPUT_POST, 'LuogoNascita');
      $Nome  = filter_input(INPUT_POST, 'Nome' );
      $Cognome  = filter_input(INPUT_POST, 'Cognome' );
      $tipoU = filter_input(INPUT_POST, 'tipoUtente');


    if ($PasswordEm == $Password2) {

      $sql = 'SELECT  COUNT(*) as counter
            FROM UTENTE
            WHERE Email="'.$Email.'"';

      $res = $pdo -> query($sql);
      $row = $res -> fetch();


      if ($row['counter'] == 0 ) {

        if ($tipoU == 'US' ) {

          try {
            $query="CALL NuovoUtente(?,?,?,?,?,?)";
            $stmt = $pdo->prepare($query);
            $stmt->bindParam(1,$Email);
            $stmt->bindParam(2,$PasswordEm);
            $stmt->bindParam(3,$DataNascita);
            $stmt->bindParam(4,$LuogoNascita);
            $stmt->bindParam(5,$Nome);
            $stmt->bindParam(6,$Cognome);

            $stmt->execute();
            $stmt->closeCursor();
          } catch (Exception $e) {
            echo "Errore nella procedura NuovoUtente(): ".$e -> getMessage();
            exit();
          }
          try {
            $query="CALL NuovoUtenteSemplice(?)";
            $stmt = $pdo->prepare($query);
            $stmt->bindParam(1,$Email);


            $stmt->execute();
            $stmt->closeCursor();
          } catch (Exception $e) {
            echo "Errore durante la procedura NuovoUtenteSemplice(): ".$e -> getMessage();
            exit();
          }

          echo "<script>alert('Registrazione avvenuta con successo, Benvenuto su E-Pool!'); window.location = './index.php';</script>";


          $data=date("Y-m-d H:i:s");
          $bulkWrite = new MongoDB\Driver\BulkWrite;
          $doc = ['avviso' => 'registrazione nuovo utente semplice',
            'utente' => $Email, 'data' => $data ];
          $bulkWrite->insert($doc);
          $manager->executeBulkWrite('epool.logEpool', $bulkWrite);

      }

      if ($tipoU == 'UD') {
        $NomeAzienda= $_POST['NomeAzienda'];
        if($NomeAzienda==""){
      echo "<script>alert('Devi selezionare un azienda!'); window.location = './formRegistrazioneUtente.html.php';</script>";
    }else{

          try {
            $query = "CALL NuovoUtente(?,?,?,?,?,?)";
            $stmt = $pdo->prepare($query);
            $stmt->bindParam(1,$Email);
            $stmt->bindParam(2,$PasswordEm);
            $stmt->bindParam(3,$DataNascita);
            $stmt->bindParam(4,$LuogoNascita);
            $stmt->bindParam(5,$Nome);
            $stmt->bindParam(6,$Cognome);

            $stmt->execute();
            $stmt->closeCursor();
          } catch (Exception $e) {
            echo "Errore durante la procedura NuovoUtente(): ".$e -> getMessage();
            exit();
          }

          try {
            $query="CALL NuovoUtenteDipendente(?,?)";
            $stmt = $pdo->prepare($query);
            $stmt->bindParam(1,$Email);
            $stmt->bindParam(2,$NomeAzienda);

            $stmt->execute();
            $stmt->closeCursor();
          } catch (Exception $e) {
            echo "Errore durante la procedura NuovoUtenteDipendente(): ".$e -> getMessage();
            exit();
          }
        }
      }

            echo "<script>alert('Registrazione avvenuta con successo, Benvenuto su E-Pool!'); window.location = './index.php';</script>";

            if (isset($_SESSION['email'])) {
              $data=date("Y-m-d H:i:s");
              $bulkWrite = new MongoDB\Driver\BulkWrite;
              $doc = ['avviso' => 'registrazione nuovo utente dipendente',
                'utente' => $Email, 'data' => $data ];
              $bulkWrite->insert($doc);
              $manager->executeBulkWrite('epool.logEpool', $bulkWrite);
            }


        } else {
          echo "<script>alert('Utente già registrato'); window.location = './formRegistrazioneUtente.html.php';</script>";
        }

         } else{
           echo "<script>alert('Le password inserite non sono uguali'); window.location = './formRegistrazioneUtente.html.php';</script>";
         }

      }
       catch(PDOException $e) {
         echo("[ERRORE] Esecuzione procedura non riuscita: ".$e->getMessage());
         exit();
       }
      } else { //se l'utente non ha cliccato il bottone "REGISTRATI"
        header("Location: formRegistrazioneUtente.html.php");
        exit();
      }

    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js" ></script>
  </body>
</html>
