<?php
//connessione al database mongodb
require 'mongodb.inc.php';
require 'db.inc.php';

try {

  session_start();
  //indirizzo di partenza selezionato dall'utente
  $partenza = $_POST['IndirizzoPartenza'];
  //id tragitto selezionato dall'utente
  $trag = $_POST['CodTragitto'];

  $query = 'CALL SelezionaTappeArrivo(?, ?)';
  $stmt = $pdo -> prepare($query);
  $stmt -> bindParam(1, $trag);
  $stmt -> bindParam(2, $partenza);
  if (!$stmt -> execute()) {
    echo "Errore nella query: ".$dbc -> error.".";
  } else {
    $resp = $stmt -> fetchAll(PDO::FETCH_ASSOC);
  }
} catch (Exception $e) {
  echo "Errore: ".$e -> getMessage();
  exit();
}

include 'formSelezionaArrivo.html.php';
