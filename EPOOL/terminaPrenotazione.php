<html>
<link rel="stylesheet" media="(min-width: 640px)" href="min-640px.css">

  <head>
    <title>Epool homepage</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" >
  </head>

<body style="background-color:WHITE;">
  <!-- inseriemnto header -->
  <div style="height: 56px">
<?php
require 'mongodb.inc.php';
require 'db.inc.php';

?>
</div>
<?php

include 'header.php';
if (isset($_POST['action']) and $_POST['action'] == 'TERMINA') {

  $email = $_SESSION['email'];
  $targa = $_POST['TargaVeicolo'];
  $indirizzo = $_POST['IndirizzoArrivo'];

  try {
    //termina la prenotazione
    $query = 'CALL terminaPrenotazione(?,?,?)';
    $stmt = $pdo -> prepare($query);
    $stmt -> bindParam(1, $email);
    $stmt -> bindParam(2, $targa);
    $stmt -> bindParam(3, $indirizzo);

    $stmt -> execute();

    include 'cordinate.php';
    //include 'index.php';
    //alert("GRAZIE PER AVER UTILIZZATO UN NOSTRO VEICOLO!");
    header("location: storicoPrenotazioni.php");

  } catch (Exception $e) {
    echo "Errore in terminaPrenotazione():".$e;
    exit();
  }

  //include "formStoricoPrenotazioni.html.php";

}
if (isset($_SESSION['email'])) {
$data=date("Y-m-d H:i:s");
$bulkWrite = new MongoDB\Driver\BulkWrite;
$doc = ['avviso' => 'termine prenotazione',
  'utente' => $_SESSION['email'], 'data' => $data , 'Targa' => $targa,
  'indirizzo' => $indirizzo];
$bulkWrite->insert($doc);
$manager->executeBulkWrite('epool.logEpool', $bulkWrite);
}

?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/bootstrap.min.js" ></script>
</body>
</html>
