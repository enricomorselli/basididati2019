<?php
require 'mongodb.inc.php';
function alert($msg) {
   echo "<script type='text/javascript'>alert('$msg');</script>";
}

require 'db.inc.php';
session_start();
$email= $_SESSION['email'];
$target_dir = "image/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {

        $uploadOk = 1;
    } else {
      echo ("<script LANGUAGE='JavaScript'>
      window.alert('Il file caricato non è un immagine.');
      window.location.href='FormUpload.html.php';
      </script>");
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {

    $uploadOk = 0;
    echo ("<script LANGUAGE='JavaScript'>
    window.alert('Immagine gia esistente');
    window.location.href='FormUpload.html.php';
    </script>");


}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 1000000) {
  echo ("<script LANGUAGE='JavaScript'>
  window.alert('Immagine troppo grande');
  window.location.href='FormUpload.html.php';
  </script>");

    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
  echo ("<script LANGUAGE='JavaScript'>
  window.alert('Errore, solo formati i JPG, JPEG, PNG & GIF sono accettati.');
  window.location.href='FormUpload.html.php';
  </script>");

    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
  echo ("<script LANGUAGE='JavaScript'>
  window.alert('Caricamento della foto non avvenuto');
  window.location.href='FormUpload.html.php';
  </script>");

// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {

      echo ("<script LANGUAGE='JavaScript'>
      window.alert('Caricamento della foto avvenuto con successo');
      window.location.href='profiloUtente.html.php';
      </script>");

        //inserire il nome della foto nella tabella foto prendendo come email la mail di sessione
        $nomefoto=$_FILES["fileToUpload"]["name"];
        $sql="INSERT INTO FOTO(EmailUtente,immagine) values ('$email','$nomefoto')";
        $res = $pdo -> query($sql);

        if (isset($_SESSION['email'])) {
        $data=date("Y-m-d H:i:s");
        $bulkWrite = new MongoDB\Driver\BulkWrite;
        $doc = ['avviso' => 'effettuato upload foto',
          'utente' => $_SESSION['email'], 'data' => $data ];
        $bulkWrite->insert($doc);
        $manager->executeBulkWrite('epool.logEpool', $bulkWrite);
        }

    } else {
        alert("Caricamento della foto non avvenuto");
    }
}
?>
