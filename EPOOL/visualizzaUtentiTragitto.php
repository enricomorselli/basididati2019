<?php
require 'mongodb.inc.php';
require 'db.inc.php';

if (isset($_POST['action']) and $_POST['action'] == "VISUALIZZA") {

  session_start();
  $email = $_SESSION['email'];
  $tipo = $_SESSION['tipo'];

  $trag = $_POST['CodTragitto'];

  try {

    if ($tipo == "UP") {
       $sql = 'CALL cercaUtentiTragittoPrem(?, ?)';
    } elseif ($tipo == "UD") {
      $sql = 'CALL cercaUtentiTragittoDip(?, ?)';
    }

    $stmt = $pdo -> prepare($sql);
    $stmt -> bindParam(1, $trag);
    $stmt -> bindParam(2, $email);
    if (!$stmt -> execute()) {
      echo "Errore nella query " . $dbc -> error. ".";
    } else {
      $resp = $stmt -> fetchAll(PDO::FETCH_ASSOC);
    }
  } catch (Exception $e) {
    echo "Errore durante la query: ".$e -> getMessage();
    exit();
  }

  include 'formUtentiTragitto.html.php';

} else {
  header("Location: index.php");
  exit();
}
